#include <stdio.h>
#include <stdlib.h>

// gcc sum.c -Wall -Werror -o sum.o && ./sum.o

void sum(int* a, int* b, int size) {
    *b = 0;
    for (int i = 0; i < size; i++) {
        *b += a[i];
    }
}

void initialize(int* v, int size) {
    for (int i = 0; i < size; i++) {
        v[i] = i;
    }
}

void print(int* v, int size) {
    printf("[");
    for (int i = 0; i < size - 1; i++) {
        printf("%d, ", v[i]);
    }
    if (size != 0) printf("%d", v[size - 1]);
    printf("]\n");
}

int main(int argc, char** argv) {
    int SIZE = 32;
    if (argc > 1) SIZE = (int) atoi(argv[1]);

    int* a = (int*) malloc(SIZE * sizeof(int));
    int* b = (int*) malloc(sizeof(int));

    initialize(a, SIZE);

    sum(a, b, SIZE);

    print(a, SIZE);
    printf("b=%d\n", *b);

    free(a);
    free(b);

    return 0;
}