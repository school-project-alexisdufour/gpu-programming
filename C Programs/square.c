#include <stdio.h>
#include <stdlib.h>

// pour compiler :
// gcc square.c -Wall -Werror -o square.o
// pour exécuter :
// ./square.o

void square(int* a, int* b, int size) {
    for (int i = 0; i < size; i++) {
        b[i] = a[i] * a[i];
    }
}

void initialize(int* v, int size) {
    for (int i = 0; i < size; i++) {
        v[i] = i;
    }
}

void print(int* v, int size) {
    printf("[");
    for (int i = 0; i < size - 1; i++) {
        printf("%d, ", v[i]);
    }
    if (size != 0) printf("%d", v[size - 1]);
    printf("]\n");
}

int main(int argc, char** argv) {
    int SIZE = 32;
    if (argc > 1) SIZE = (int) atoi(argv[1]);

    int* a = (int*) malloc(SIZE * sizeof(int));
    int* b = (int*) malloc(SIZE * sizeof(int));

    initialize(a, SIZE);

    square(a, b, SIZE);

    print(a, SIZE);
    print(b, SIZE);

    free(a);
    free(b);

    return 0;
}