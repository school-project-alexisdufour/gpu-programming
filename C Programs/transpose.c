#include <stdlib.h>
#include <stdio.h>

// gcc transpose.c -Wall -Werror -o transpose.o && ./transpose.o

void transpose(int* mat1, int* mat2, int dim1, int dim2) {
    for (int y = 0; y < dim1; y++) {
        for (int x = 0; x < dim2; x++) {
            mat2[x * dim1 + y] = mat1[y * dim2 + x];
        }
    }
}

void initialize(int* mat, int dim1, int dim2) {
    for (int y = 0; y < dim1; y++) {
        for (int x = 0; x < dim2; x++) {
            mat[y * dim2 + x] = y * dim2 + x;
        }
    }
}

void printRow(int* row, int size) {
    printf("[");
    for (int x = 0; x < size - 1; x++) {
        printf("%d, ", row[x]);
    }
    if (size != 0) printf("%d", row[size - 1]);
    printf("]\n");
}

void print(int* mat, int dim1, int dim2) {
    printf("[\n");
    for (int y = 0; y < dim1; y++) {
        printf("\t");
        printRow(&mat[y * dim2], dim2);
    }
    printf("]\n");
}

int main(int argc, char** argv) {
    int dim1 = 8;
    int dim2 = 8;
    if (argc > 1) dim1 = (int) atoi(argv[1]);
    if (argc > 2) dim2 = (int) atoi(argv[2]);

    int SIZE = dim1 * dim2;

    int* mat1 = (int*) malloc(SIZE * sizeof(int));
    int* mat2 = (int*) malloc(SIZE * sizeof(int));

    initialize(mat1, dim1, dim2);

    transpose(mat1, mat2, dim1, dim2);

    print(mat1, dim1, dim2);
    print(mat2, dim2, dim1);

    free(mat1);
    free(mat2);

    return 0;
}