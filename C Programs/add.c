#include <stdio.h>
#include <stdlib.h>

// gcc add.c -Wall -Werror -o add.o && ./add.o

void add(int* a, int* b, int* c, int size) {
    for (int i = 0; i < size; i++) {
        c[i] = a[i] + b[i];
    }
}

void initialize(int* v, int size) {
    for (int i = 0; i < size; i++) {
        v[i] = i;
    }
}

void print(int* v, int size) {
    printf("[");
    for (int i = 0; i < size - 1; i++) {
        printf("%d, ", v[i]);
    }
    if (size != 0) printf("%d", v[size - 1]);
    printf("]\n");
}

int main(int argc, char** argv) {
    int SIZE = 32;
    if (argc > 1) SIZE = (int) atoi(argv[1]);

    int* a = (int*) malloc(SIZE * sizeof(int));
    int* b = (int*) malloc(SIZE * sizeof(int));
    int* c = (int*) malloc(SIZE * sizeof(int));

    initialize(a, SIZE);
    initialize(b, SIZE);

    add(a, b, c, SIZE);

    print(a, SIZE);
    print(b, SIZE);
    print(c, SIZE);

    free(a);
    free(b);
    free(c);

    return 0;
}