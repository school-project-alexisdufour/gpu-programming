#include <stdio.h>
#include <stdlib.h>

// gcc decaler.c -Wall -Werror -o decaler.o && ./decaler.o

void decaler(int* a, int size) {
    if (size == 0) return;
    int last = a[size-1];
    for (int i = size-1; i >= 0; i--) {
        a[i] = a[i-1];
    }
    a[0] = last;
}

void initialize(int* v, int size) {
    for (int i = 0; i < size; i++) {
        v[i] = i;
    }
}

void print(int* v, int size) {
    printf("[");
    for (int i = 0; i < size - 1; i++) {
        printf("%d, ", v[i]);
    }
    if (size != 0) printf("%d", v[size - 1]);
    printf("]\n");
}

int main(int argc, char** argv) {
    int SIZE = 32;
    if (argc > 1) SIZE = (int) atoi(argv[1]);

    int* a = (int*) malloc(SIZE * sizeof(int));

    initialize(a, SIZE);

    print(a, SIZE);

    decaler(a, SIZE);

    print(a, SIZE);

    free(a);

    return 0;
}